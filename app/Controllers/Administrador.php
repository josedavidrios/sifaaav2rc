<?php namespace App\Controllers;

use App\Models\UserModel;

class Administrador extends BaseController
{





    public function __construct()
    {

        helper('utility');

        if (!session('usuario')) {

            return redirect()->to(base_url());
        }

    }

    function index()
    {





        $dat['css'] = [''];
        $dat['title'] = "Dashboard";
        $dat['content'] = "blank";

        echo view('dashboard/inc/layout', $dat);

    }




    function vistaUsuarios(){


        $dat['css'] = [''];
        $dat['title'] = "Usuario";
        $dat['content'] = "usuarios/usuario";


        /*
        $poblaciones = new Poblacion();
        $dat['poblaciones'] =  $poblaciones->consultar();
        */


        echo view('dashboard/inc/layout', $dat);

    }


    function vistaCausacion(){


        $dat['css'] = [''];
        $dat['title'] = "Usuario";
        $dat['content'] = "facturacion/causar";


        /*
        $poblaciones = new Poblacion();
        $dat['poblaciones'] =  $poblaciones->consultar();
        */


        echo view('dashboard/inc/layout', $dat);

    }


}
