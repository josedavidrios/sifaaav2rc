<?php namespace App\Controllers;

use App\Models\BarrioModel;


class Barrio extends BaseController
{



    public $barrios;


    public function __construct()
    {

        $this->barrios = new BarrioModel();

    }


    function consultarBarriosPorPoblacion($codigo=null){


        return $this->barrios->consultarBarriosPorPoblacion($codigo);

    }


    function consultarBarriosPorPoblacionAjax(){


        $codigoPoblacion =  $this->request->getGetPost("poblacion");
        echo json_encode($this->consultarBarriosPorPoblacion($codigoPoblacion));





    }


}
