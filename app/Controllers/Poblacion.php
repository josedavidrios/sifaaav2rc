<?php namespace App\Controllers;

use App\Models\PoblacionModel;
use App\Models\UserModel;

class Poblacion extends BaseController
{



    public $poblaciones;


    public function __construct()
    {

        $this->poblaciones = new PoblacionModel();

    }


    function consultar($codigo=null){


        return $this->poblaciones->consultar($codigo);

    }

    function consultarAjax(){



        $codigo =  $this->request->getGetPost("poblacion");



        echo json_encode($this->consultar($codigo));


    }



}
