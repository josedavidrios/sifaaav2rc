<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class BarrioModel extends Model
{


    protected $table      = 'barrios b';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';



    function consultar($codigo){


        if (!is_null($codigo)){


            $this->where("codigo",$codigo);


        }

        return $this->findAll();


    }


    function consultarBarriosPorPoblacion($codigo){


        $this->select("b.nombre,b.codigo")
             ->join("poblaciones p", "p.codigo = codigo_poblacion","INNER");


        if (!is_null($codigo)){


            $this->where("p.codigo",$codigo);


        }





        return $this->findAll();


    }



}
