<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 20/11/2019
 * Time: 5:21 PM
 */

namespace App\Models;


use CodeIgniter\Model;

class BaseModel extends  Model
{


    public $sql;

    public function __construct($table)
    {
       $this->sql = \Config\Database::connect()->table($table);

    }


}