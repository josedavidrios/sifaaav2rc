<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class FacturaModel extends Model
{


    protected $table      = 'facturas f';
    protected $returnType = 'object';


    protected $primaryKey = 'codigo';

    protected $allowedFields = ['codigoCausacion', 'codigoCausacion','codigoCausacion','estrato','valor','ruta','estado','fechaCreacion','admin'];



    function consultar($codigo){


        if (!is_null($codigo)){


            $this->where("codigo",$codigo);


        }

        return $this->findAll();


    }



    /*



        $sql = "SELECT u.codigo, u.nombres, u.direccion,e.estrato, e.codigo AS codigo_estrato, u.codigo_tarifa,


cac.total + cal.total + cas.total AS total

FROM usuarios u, barrios b, estratos e,

tarifas t, subtarifas_acueducto cac, subtarifas_alcantarillado cal, subtarifas_aseo cas

WHERE u.codigo_barrio = b.codigo
AND u.codigo_estrato = e.codigo

 AND u.causar =1

AND u.codigo_tarifa = t.codigo

AND t.codigo_acueducto = cac.codigo
AND t.codigo_alcantarillado = cal.codigo
AND t.codigo_aseo = cas.codigo


GROUP BY u.codigo";






     * */


}
