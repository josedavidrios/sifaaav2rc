<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class PoblacionModel extends Model
{


    protected $table      = 'poblaciones';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';



    function consultar($codigo){


        if (!is_null($codigo)){


            $this->find("codigo",$codigo);


        }

        return $this->findAll();


    }


}
