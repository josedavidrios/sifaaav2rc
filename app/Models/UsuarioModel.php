<?php
/**
 * Created by PhpStorm.
 * Administrador: jose.riosp
 * Date: 26/11/2019
 * Time: 10:19 AM
 */

namespace App\Models;


use CodeIgniter\Model;

class UsuarioModel extends Model
{


    protected $table      = 'usuarios u';
    protected $returnType = 'object';
    protected $primaryKey = 'codigo';

    protected $allowedFields = ['cedula', 'telefono','nombres','direccion','catastro','ruta','codigoBarrio','codigoEstrato','codigoTarifa','codigoPoblacion','admin'];


    function consultar($codigo=null){


        if (!is_null($codigo)){


            $this->find($codigo);


        }

        return $this->orderBy('codigo','desc')->findAll();


    }


    function crear($dat){



        return $this->insert($dat);



    }


    function filtrar($nombre){


    	return $this->like('nombres',$nombre,'both')->findAll();


	}


    function consultarUsuariosCausarsion(){


        //u.codigo, u.nombres, u.direccion,e.estrato, e.codigo AS codigo_estrato, u.codigo_tarifa

        $this->select("u.codigo,
                             u.nombres,
                             u.direccion,
                             u.codigoEstrato, 
                             u.codigoTarifa,
                             t.codigo AS codigoTarifa,
                             u.codigoEstrato,
                             
                             
                             
                             tac.total  AS valorTarifaAcueducto, tal.total AS valorTarifaAlcantarillado,tas.total AS valorTarifaAseo",true)



            ->join("barrios b","b.codigo = u.codigoBarrio")
            ->join("estratos e","e.codigo = u.codigoEstrato")
            ->join("tarifas t","t.codigo = u.codigoTarifa")

            ->join("tarifasAcueducto tac","tac.codigo = t.tarifaAcueducto")
            ->join("tarifasAlcantarillado tal","tal.codigo = t.tarifaAlcantarillado")
            ->join("tarifasAseo tas","tas.codigo = t.tarifaAseo")


            ->where("u.causar",1);




        return $this->findAll();

    }



    function causar(){



    }








}
