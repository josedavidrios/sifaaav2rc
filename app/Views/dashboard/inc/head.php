<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?=$title?></title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="<?=asset_url('img/icon.ico')?>" type="image/x-icon"/>

    <!-- Fonts and icons -->
    <script src="<?=asset_url('js/plugin/webfont/webfont.min.js')?>"></script>

    <!-- CSS Files -->
    <link rel="stylesheet" href="<?=asset_url('css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=asset_url('css/azzara.min.css')?> ">

    <!-- CSS Just for demo purpose, don't include it in your project -->
    <link rel="stylesheet" href="<?=asset_url('css/demo.css')?>">

    <link rel="stylesheet" href="<?=asset_url('css/custom.css')?>">

    <link rel="stylesheet" href="<?=asset_url('css/fonts.css')?>">

    <script src="<?=asset_url('js/config.js')?>"></script>
    <script src="<?=asset_url('js/vue.js')?>"></script>
    <script src="<?=asset_url('js/axios.min.js')?>"></script>


</head>