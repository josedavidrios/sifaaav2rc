<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-11 col-sm-11">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" v-on:keyup="filtarUsuarios" v-model="filtro" placeholder="Buscar...." class="form-control text-uppercase">

                    </div>
                </form>


            </div>

            <div class="col-md-1 col-sm-1">


                <a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal"
                   data-target="#exampleModal">
                    <span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
                </a>


            </div>

        </div>


    </div>


</div>

<br>

<div class="row">


    <div class="col-md-12">


        <div class="card">


            <!--
            <div class="card-header">
                <div class="card-title">Hoverable Table</div>
            </div>

            -->

            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">CÓD</th>
                        <th scope="col">NOMBRES</th>
                        <th scope="col">DIRECCIÓN</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr v-for="item in usuarios">
                        <td>{{item.codigo}}</td>
                        <td>{{item.nombres}}</td>
                        <td>{{item.direccion}}</td>

                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


    </div>





</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">


        <form method="post" v-on:submit.prevent="registrarUsuario">

            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">


                    <div class="row">
                        <div class="col-md-12">


                            <div class="card-body">


                                <div class="form-group">
                                    <label>Cédula</label>
                                    <input type="number" v-model="cedula" class="form-control" placeholder="Cédula">

                                </div>
                                <div class="form-group">
                                    <label>Nombres </label>
                                    <input type="text" v-model="nombres" class="form-control" placeholder="Nombres">
                                </div>


                                <div class="form-group">
                                    <label>Población </label>


                                    <select v-model="poblacion" class="form-control" @change="seleccionarPoblacion">


                                        <option v-for="item in poblaciones" v-bind:value="item.codigo">
                                            {{ item.nombre }}
                                        </option>


                                    </select>


                                </div>


                                <div class="form-group">
                                    <label>Barrio </label>

                                    <select v-model="barrio" class="form-control">


                                        <option v-for="item in barrios" v-bind:value="item.codigo">
                                            {{ item.nombre }}
                                        </option>


                                    </select>


                                </div>

                                <div class="form-group">
                                    <label>Dirección </label>
                                    <input type="text" v-model="direccion" class="form-control" placeholder="Dirección">
                                </div>


                                <div class="form-group">
                                    <label>Teléfono </label>
                                    <input type="text" v-model="telefono" class="form-control" placeholder="Teléfono">
                                </div>


                                <div class="form-group">
                                    <label>Estato </label>
                                    <select class="form-control" v-model="estrato" id="">
                                        <option value="1">UNO</option>
                                        <option value="2">DOS</option>
                                        <option value="3">TRES</option>
                                        <option value="4">CUATRO</option>
                                    </select>
                                </div>


                            </div>

                        </div>


                    </div>

                </div>
                <div class="modal-footer">

                    <input type="reset" class="btn btn-secondary" data-dismiss="modal">
                    <input type="submit" class="btn btn-primary" value="Registrar">


        </form>
    </div>
</div>
</div>


<script>


    new Vue({
        el: '#app',


        created: function () {

          //  this.consultarUsuarios();
            this.consultarPoblaciones();


        },


        data: {

            barrios: [],
            poblaciones: [],
            usuarios: [],


            filtro:'',

            cedula: '',
            nombres: '',
            poblacion: '',
            barrio: 'SELECCIONE',
            direccion: '',
            telefono: '',
            estrato: ''


        },
        methods: {
            consultarPoblaciones: function () {


                const params = new URLSearchParams();
                params.append('poblacion', this.selected);


                axios.post(BASE_URL + 'Poblacion/consultarAjax').then(response => {
                    this.poblaciones = response.data
                });
            },
            seleccionarPoblacion(event) {

                const params = new URLSearchParams();
                params.append('poblacion', event.target.value);

                axios.post(BASE_URL + 'Barrio/consultarBarriosPorPoblacionAjax', params).then(response => {
                    this.barrios = response.data
                });


            }, consultarUsuarios: function () {


                axios.get(BASE_URL + 'usuario/consultarAjax').then(response => {
                    this.usuarios = response.data;
                });






            },

            filtarUsuarios:function(event){


                const params = new URLSearchParams();
                params.append('nombres', event.target.value);

                axios.post(BASE_URL + 'usuario/filtrar',params).then(response => {
                    this.usuarios = response.data;

                   console.log(this.usuarios.length);

                });





			},

            registrarUsuario: function () {


                /*



                    cedula:this.cedula,
                    nombres:this.nombres,
                    poblacion:this.poblacion,
                    barrio:this.barrio,
                    direccion:this.direccion
                * */


                const params = new URLSearchParams();
                params.append('cedula', this.cedula);
                params.append('nombres', this.nombres);
                params.append('poblacion', this.poblacion);
                params.append('barrio', this.barrio);
                params.append('direccion', this.direccion);
                params.append('telefono', this.telefono);
                params.append('estrato', this.estrato);


                axios.post(BASE_URL + 'Usuarios/crear', params
                ).then(response => {


                    $('#exampleModal').modal('toggle');


                    swal({
                        title: "Mensaje",
                        text: "El usuarios se ha creadp correctamente",
                        button: "Aceptar",
                    });


                });


            }

        }
    });


</script>



