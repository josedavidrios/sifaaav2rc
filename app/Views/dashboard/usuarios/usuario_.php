
<div class="row">


    <div class="container-fluid">

        <div class="row">
            <div class="col-md-3">
                <form class="navbar-left navbar-form nav-search mr-md-12">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="submit" class="btn btn-search pr-1">
                                <i class="fa fa-search search-icon"></i>
                            </button>
                        </div>
                        <input type="text" placeholder="Buscar...." class="form-control">
                    </div>
                </form>


            </div>

            <div class="col-md-9 ">


                <a href="#" class="pull-right btn btn-info btn-border btn-round btn-sm" data-toggle="modal" data-target="#exampleModal">
                    <span class="btn-label"><i class="fa fa-user-plus"></i></span>Nuevo
                </a>


            </div>

        </div>




    </div>




</div>

<br>

<div class="row">





    <div class="col-md-12">




        <div class="card">
            <div class="card-header">
                <div class="card-title">Hoverable Table</div>
            </div>
            <div class="card-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">First</th>
                        <th scope="col">Last</th>
                        <th scope="col">Handle</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Jacob</td>
                        <td>Thornton</td>
                        <td>@fat</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td colspan="2">Larry the Bird</td>
                        <td>@twitter</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>


    </div>

</div>



<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">


                <div class="row">
                    <div class="col-md-12">


                        <div class="card-body">
                            <div class="form-group">
                                <label >Cédula</label>
                                <input type="email" class="form-control" placeholder="Cédula">

                            </div>
                            <div class="form-group">
                                <label>Nombres </label>
                                <input type="text" class="form-control" placeholder="Nombres">
                            </div>


                            <div class="form-group">
                                <label for="password">Dirección  </label>
                                <input type="text" class="form-control" placeholder="Dirección">
                            </div>


                            <div class="form-group">
                                <label for="password">Población  </label>


                                <select class="form-control" name="" onchange="consultarBarrios(this.value)" id="">




                                    <?php



                                    foreach ($poblaciones as $poblacion){

                                        ?>
                                        <option value="<?=$poblacion->codigo?>"> <?=$poblacion->nombre?> </option>


                                        <?php

                                    }
                                    ?>


                                </select>


                            </div>


                            <div class="form-group">
                                <label for="password">Barrio  </label>
                                <select class="form-control" name="" id="barrrios">


                                </select>
                            </div>


                        </div>

                    </div>



                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Registrar</button>
            </div>
        </div>
    </div>
</div>


<script>



    function consultarBarrios(codigo){




        $.post( "http://localhost/CodeIgniter4-4.0.0-rc.3/public/Barrios/consultarBarriosPorPoblacionAjax", { poblacion: codigo })
            .done(function( data ) {
                $("#barrrios").html(data);

            })




    }






</script>
