<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title> <?=$title?> </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <link rel="icon" href="<?=asset_url('img/icon.ico')?>" type="image/x-icon"/>

    <!-- Fonts and icons -->
    <script src="<?=asset_url('js/plugin/webfont/webfont.min.js')?>"></script>


    <!-- CSS Files -->
    <link rel="stylesheet" href="<?=asset_url('css/bootstrap.min.css')?>">
    <link rel="stylesheet" href="<?=asset_url('css/azzara.min.css')?>">
    <link rel="stylesheet" href="<?=asset_url('css/fonts.css')?>">

</head>
<body class="login">
<div class="wrapper wrapper-login">
    <div class="container container-login animated fadeIn">
        <h3 class="text-center">Inicio de Sesión</h3>


        <form action="<?=base_url('Sesion/iniciar')?>" method="post">

        <div class="login-form">
            <div class="form-group">
                <label for="username" class="placeholder"><b>Usuario</b></label>
                <input id="username" name="usuario" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="password" class="placeholder"><b>Contraseña</b></label>
                <a href="#" class="link float-right">¿ Olvidó su contraseña ? </a>
                <div class="position-relative">
                    <input id="password" name="clave" type="password" class="form-control" required>
                    <div class="show-password">
                        <i class="flaticon-interface"></i>
                    </div>
                </div>
            </div>
            <div class="form-group form-action-d-flex mb-3">


                <input class="btn btn-primary offset-md-7 col-md-5 float-right mt-3 mt-sm-0 fw-bold" value="Iniciar" type="submit">


            </div>


        </form>

        </div>
    </div>

    <div class="container container-signup animated fadeIn">
        <h3 class="text-center">Sign Up</h3>
        <div class="login-form">
            <div class="form-group">
                <label for="fullname" class="placeholder"><b>Fullname</b></label>
                <input  id="fullname" name="fullname" type="text" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="email" class="placeholder"><b>Email</b></label>
                <input  id="email" name="email" type="email" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="passwordsignin" class="placeholder"><b>Password</b></label>
                <div class="position-relative">
                    <input  id="passwordsignin" name="passwordsignin" type="password" class="form-control" required>
                    <div class="show-password">
                        <i class="flaticon-interface"></i>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="confirmpassword" class="placeholder"><b>Confirm Password</b></label>
                <div class="position-relative">
                    <input  id="confirmpassword" name="confirmpassword" type="password" class="form-control" required>
                    <div class="show-password">
                        <i class="flaticon-interface"></i>
                    </div>
                </div>
            </div>
            <div class="row form-sub m-0">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" name="agree" id="agree">
                    <label class="custom-control-label" for="agree">I Agree the terms and conditions.</label>
                </div>
            </div>
            <div class="row form-action">
                <div class="col-md-6">
                    <a href="#" id="show-signin" class="btn btn-danger btn-link w-100 fw-bold">Cancel</a>
                </div>
                <div class="col-md-6">
                    <a href="#" class="btn btn-primary w-100 fw-bold">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?=asset_url('js/core/jquery.3.2.1.min.js')?>"></script>
<script src="<?=asset_url('js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js')?>"></script>
<script src="<?=asset_url('js/core/popper.min.js')?>"></script>
<script src="<?=asset_url('js/core/bootstrap.min.js')?>"></script>
<script src="<?=asset_url('js/ready.js')?>"></script>
</body>
</html>